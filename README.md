# Review Downloader

Review downloader project
Last updated - March 11th 2017
Background
Some of our customers have requested that we handle reviews in our products (e.g. App store reviews, Amazon
reviews). As a first step, we would like to be able to download the reviews.

User interface

Create a website where a user can input a URL from a reviews site
When the user submits the URL account, your server should download the data from the reviews and create a CSV
file in the following format

UserName - Name of user who wrote the review
Date - Date of the review
Star rating - Number of stars
Review Comment
Link - Direct link to the review (where available)
UserName Date Star
rating
Review Comment Link


Basic version
Your site should support reviews from Google Play and Apple Store. Here are some examples
Sample URL that user will input into your site API
Apple Store https://itunes.apple.com/us/app/grubhub-food-delivery-ta
keout/id 302920553 ?mt=8
https://itunes.apple.com/rss/custome
rreviews/id= 302920553 /json
Google Play https://play.google.com/store/apps/details?id= com.eero.
android
Unofficial API
There might be other APIs which
work better, you can feel free to
choose any one
Download as many reviews as possible i.e. if the app has thousands of reviews then the CSV should also contain
thousands of reviews

Evaluation criteria
Send me the following when you are complete
● URL to the website
● Source code